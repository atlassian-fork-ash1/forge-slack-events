import slackify from 'slackify-html';

export function formatUserLink(activityItem, actor) {
  const site = findContainer(activityItem, 'SITE');
  return `<${site.url}/people/${actor.accountId}|${actor.displayName}>`;
}

export function findContainer(activityItem, type) {
  const container = activityItem.containers.find(obj => (obj.type === type));
  if (!container) {
    const err = `No container with type ${type} found in ${JSON.stringify(activityItem.containers)}`;
    error(err);
    throw new Error(err);
  }
  return container;
}

export function html2mrkdwn(html) {
  return slackify(html);
}