import { slackOAuthBearerToken } from '../config'
import { error, debug } from '../util/logger';

export async function apiRequest(resource, body) {
  let opts = {};
  if (body) {
    opts = Object.assign({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    });
  }
  opts.headers = Object.assign(opts.headers, {
    Authorization: `Bearer ${slackOAuthBearerToken}`
  });
  const response = await api.fetch(`https://slack.com/api/${resource}`, opts);
  if (!response.ok) {
    const err = `Error invoking ${resource} (Slack): ${response.status} ${response.statusText}`;
    error(err);
    throw new Error(err);
  }
  const responseBody = await response.json();
  debug(`Response from Slack: ${JSON.stringify(responseBody)}`);
  return responseBody;
}